import os
import time
import json
import uuid
import string
import random

from decimal import Decimal
from pathlib import Path
from datetime import datetime
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError

from confluent_kafka.avro import AvroProducer


def random_str() -> str:
    length = random.randint(2, 10)
    text = ''.join(random.choices(string.ascii_lowercase, k=length))
    return text.title()


def request(link: str, body: str = '{}'):
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    req = Request(link, body.encode(), headers, method='POST')
    try:
        with urlopen(req) as resp:
            response = json.load(resp)
    except HTTPError as err:
        return int(err.code), err.reason
    except URLError as err:
        return 1, err.reason
    except BaseException as err:
        return 0, str(err)
    else:
        return resp.status, response


def add_kafka_connects():
    connect_host = os.environ.get('CONNECT_REST_ADVERTISED_HOST_NAME', 'kafka-connect')
    connect_port = os.environ.get('CONNECT_REST_PORT', '8083')
    files = list(Path('connect').glob('*.json'))
    while files:
        connect = files.pop(0)
        url = f'http://{connect_host}:{connect_port}/connectors'
        status, resp = request(url, connect.read_text())
        print(f'Connect: url={url}, file={connect.name}, status={status}, response={resp}')
        if status in [0, 1]:
            files.append(connect)
        time.sleep(2)


def start_producer(records: int = 0):
    kafka_host = os.environ.get('KAFKA_EXTERNAL_HOSTNAME', 'kafka-broker')
    kafka_port = os.environ.get('KAFKA_EXTERNAL_PORT', '9092')
    kafka_topic_name = os.environ.get('TOPIC_NAME', 'test.topic')
    kafka_schema_registry_server = os.environ.get('SCHEMA_REGISTRY_LISTENERS', 'http://kafka-schema-registry:8081')

    key_schema = '''{
    "name": "TestKey", "namespace": "test", "type": "record", 
    "fields": [{"name": "id", "type": "string", "logicalType": "uuid"}]
    }'''
    value_schema = '''{
    "name": "TestValue", "namespace": "test", "type": "record", 
    "fields": [
        {"name": "boolean", "type": "boolean"},
        {"name": "int", "type": "int"},
        {"name": "long", "type": "long"},
        {"name": "float", "type": "float"},
        {"name": "double", "type": "double"},
        {"name": "bytes", "type": "bytes"},
        {"name": "string", "type": "string"},
        {"name": "decimal", "type": {"type": "bytes", "logicalType": "decimal", "precision": 4, "scale": 4}},
        {"name": "uuid", "type": {"type": "string", "logicalType": "uuid"}},
        {"name": "date", "type": {"type": "int", "logicalType": "date"}},
        {"name": "timemillis", "type": {"type": "int", "logicalType": "time-millis"}},
        {"name": "timemicros", "type": {"type": "long", "logicalType": "time-micros"}},
        {"name": "timestampmillis", "type": {"type": "long", "logicalType": "timestamp-millis"}},
        {"name": "timestampmicros", "type": {"type": "long", "logicalType": "timestamp-micros"}},
        {"name": "localtimestampmillis", "type": {"type": "long", "logicalType": "local-timestamp-millis"}},
        {"name": "localtimestampmicros", "type": {"type": "long", "logicalType": "local-timestamp-micros"}},
        {"name": "fixed", "type": {"name": "fixed", "type": "fixed", "size": 16}},
        {"name": "enum", "type": {"name": "enum", "type": "enum", "symbols" : ["Male", "Female", "X"]}},
        {"name": "arrayP", "type": {"type": "array", "items" : "int", "default": []}},
        {"name": "arrayR", "type": {"type": "array", "items" : 
            {"name":"arrayR", "type":"record", "fields": [{"name": "name", "type": "string"}]}, "default": []}},
        {"name": "record", "type": {"name": "record", "type": "record", "fields": [
            {"name": "first", "type": "string"}, {"name": "last", "type": "string"}]}},
        {"name": "map", "type": {"type": "map", "values" : "string", "default": {}}}
    ]
    }'''
    kafka_producer = AvroProducer(
        config={'bootstrap.servers': f'{kafka_host}:{kafka_port}', 'schema.registry.url': kafka_schema_registry_server},
        default_key_schema=key_schema,
        default_value_schema=value_schema,
    )

    while records:
        key = {'id': uuid.uuid4().hex}
        value = {
            "boolean": random.choice([True, False]),
            "int": random.randint(0, 100),
            "long": random.randint(0, 100),
            "float": random.random(),
            "double": random.random(),
            "bytes": random.randbytes(16),
            "string": random_str(),
            "decimal": Decimal(f'{random.random():.4f}'),
            "uuid": uuid.uuid4().hex,
            "date": datetime.utcnow().date(),
            "timemillis": datetime.utcnow().time(),
            "timemicros": datetime.utcnow().time(),
            "timestampmillis": datetime.utcnow(),
            "timestampmicros": datetime.utcnow(),
            "localtimestampmillis": datetime.utcnow(),
            "localtimestampmicros": datetime.utcnow(),
            "fixed": random.randbytes(16),
            "enum": random.choice(['Male', 'Female', 'X']),
            "arrayP": [random.randint(0,100) for _ in range(10)],
            "arrayR": [{'name': random_str()} for _ in range(10)],
            "record": {'first': random_str(), 'last': random_str()},
            "map": {'dog': random_str(), 'cat': random_str()},
        }
        kafka_producer.produce(topic=kafka_topic_name, key=key, value=value)
        kafka_producer.flush()
        print('Sent:', key, value)
        time.sleep(2)
        records -= 1


if __name__ == '__main__':
    time.sleep(30)
    start_producer(2)
    add_kafka_connects()
    start_producer(1000)
