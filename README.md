# Testing
This project demonstrates how to use SMTs with JdbcSinkConnector to handle complex Avro structures.


## Run
```shell
git clone ...
cd docker
docker-compose up
```

## Monitoring
### Broker
```shell
docker exec -it dp_kafka_broker bash;
kafka-topics --bootstrap-server kafka-broker:9092 --list;
kafka-topics --bootstrap-server kafka-broker:9092 --delete --topic test.topic;
kafka-console-consumer --bootstrap-server kafka-broker:9092 --topic test.topic --property print.key=true --from-beginning;
```

### Connect
```shell
docker exec -it dp_producer_app bash
curl http://kafka-connect:8083/connectors/ -X GET 
curl http://kafka-connect:8083/connectors/sink-postgres-raw -X DELETE
curl http://kafka-connect:8083/connectors/sink-postgres-r2j -X DELETE
curl http://kafka-connect:8083/connectors/sink-postgres-r2r -X DELETE
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/sink-postgres-raw.json
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/sink-postgres-r2j.json
curl http://kafka-connect:8083/connectors/ -X POST -H "Content-Type: application/json" -d @connect/sink-postgres-r2r.json
```

### PostgreSQL
```shell
docker exec -it dp_db_postgres psql default admin 
\l
SELECT * FROM pg_stat_all_tables WHERE schemaname not in ('pg_catalog', 'pg_toast', 'information_schema');
```



## Problems
The main problem is to move data with complex structure from MongoDB to JDBC (Postgres).
There are three ways to solve this problem:
1. **Avro Schema**
   1. Source: `Debezium CDC Connector -> SMT (ExtractNewDocumentState) -> AvroConverter (schema.enable=true)`;
   2. Sink: `AvroConverter (schema.enable=true) -> Record2JsonStringConverter -> JDBC Connector`;  

2. **Avro Schema**
   1. Source: `Debezium CDC Connector -> SMT (ExtractNewDocumentState) -> AvroConverter (schema.enable=true)`;
   2. Sink: `AvroConverter (schema.enable=true) -> Record2RowConverter -> JDBC Connector`;  

3. **Raw JSON**
   1. Source: `Debezium CDC Connector -> SMT (ExtractNewDocumentState) -> JsonConverter (schema.enable=true)`;
   2. ksqlDB processing:  
   ```sql
   CREATE STREAM raw_conditions (records VARCHAR) WITH (KAFKA_TOPIC='mongo.conditions', VALUE_FORMAT='KAFKA');
   SET 'auto.offset.reset' = 'earliest';
   CREATE STREAM wrapped_conditions WITH (FORMAT='AVRO') AS SELECT * FROM raw_conditions;
   ```
   3. Sink: `AvroConverter (schema.enable=true) -> Record2JsonStringConverter -> JDBC Connector`; 
- [Record2JsonStringConverter SMT repo](https://github.com/an0r0c/kafka-connect-transform-tojsonstring);  
- [Record2RawConverter SMT repo](https://github.com/kazanzhy/kafka-connect-transform-record2row);  
- [Confluent Forum](https://forum.confluent.io/t/storing-topic-data-in-a-single-column-as-a-json-in-postgres/1207);

